package ca.guanyi.viewlearningapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.List;
import Data.Student;
import Data.StudentLocalData;
import ca.guanyi.viewlearningapp.R;

public class MainActivity extends AppCompatActivity {
    private List<Student> students;
    ListView studentListView;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        students = new StudentLocalData().getStudentData();

        studentListView = (ListView)findViewById(R.id.activity_main_list_view);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, students);
        studentListView.setAdapter(adapter);

        /*Basic steps
          1, Add ListView into Activity, make sure set an id
          2, getData in an array or List (List is an interface which implemented by ArrayList)
          3, get the ListView variable by R.id.
          4, get the listView row view id. Android provided a default listView item layout to handel text
            ,use android.R.layout.simple_list_item_1
          5, create an ArrayAdapter. There are 5  overloaded constructors(activity context and id in step 4 are must)
             then array of any kinds of object or List of object (Collection interface will not work)
          6, listView.setAdapter(adapter);
          */
    }
}
