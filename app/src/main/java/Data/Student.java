package Data;

/**
 * Created by Guanyi on 10/8/2016.
 */

public class Student {
    private String firstName;
    private String lastName;
    private String phone;

    public Student(String FirstName, String LastName, String Phone) {
        firstName = FirstName;
        lastName = LastName;
        phone = Phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return  "firstName : " + firstName + '\n' +
                "lastName : " + lastName + '\n' +
                "phone : " + phone;
    }
}
