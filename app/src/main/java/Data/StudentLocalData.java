package Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guanyi on 10/8/2016.
 */

public class StudentLocalData implements StudentData {
    private List<Student> students;

    public StudentLocalData() {
        students = new ArrayList<>();
    }

    public List<Student> getStudentData() {
        students.add(new Student("Jone", "Jackson", "604-111-1111"));
        students.add(new Student("Alice", "White", "604-222-2222"));
        students.add(new Student("Peter", "Pan", "604-333-3333"));
        students.add(new Student("Cindy", "Miller", "604-444-4213"));
        students.add(new Student("Tracy", "Hanson", "604555-1230"));
        students.add(new Student("Kyle", "Wu", "433-211-7890"));
        students.add(new Student("Pam", "Anderson", "250-784-5623"));
        students.add(new Student("Wendy", "Liu", "250-456-1478"));
        students.add(new Student("Will", "Lee", "604-895-6687"));
        students.add(new Student("Ali", "Sa", "604-587-4187"));
        students.add(new Student("Sandra", "Nicholson", "250-765-8999"));
        students.add(new Student("Joe", "Bo", "250-632-5896"));
        students.add(new Student("Mary", "Green", "604-236-9152"));
        return students;
    }

}
