package Data;

import java.util.List;

/**
 * Created by Guanyi on 10/8/2016.
 */

public interface StudentData {
    List<Student> getStudentData();
}
